# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import invoice


def register():
    Pool.register(
        invoice.InvoiceDiscountWizardStart,
        module='account_invoice_discount_wizard', type_='model')
    Pool.register(
        invoice.InvoiceDiscountWizard,
        module='account_invoice_discount_wizard', type_='wizard')

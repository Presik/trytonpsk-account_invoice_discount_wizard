# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import ModelView, fields
from trytond.pool import Pool
from trytond.wizard import Wizard, StateTransition, StateView, Button
from trytond.transaction import Transaction


class InvoiceDiscountWizardStart(ModelView):
    'Invoice Discount Wizard Start'
    __name__ = 'account_invoice.discount_wizard.start'
    discount = fields.Float('Discount', digits=(2, 2), required=True)


class InvoiceDiscountWizard(Wizard):
    'Invoice Discount Wizard'
    __name__ = 'account_invoice.discount_wizard'
    start = StateView('account_invoice.discount_wizard.start',
        'account_invoice_discount_wizard.invoice_discount_wizard_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        Invoice = Pool().get('account.invoice')
        invoices = Invoice.browse(Transaction().context['active_ids'])
        invoices = [i for i in invoices if i.state == 'draft']
        for invoice in invoices:
            for line in invoice.lines:
                if line.type != 'line':
                    continue
                value = self.start.discount / 100
                line.discount_rate = Decimal(value)
                line.on_change_discount_rate()
                line.save()
            invoice.on_change_taxes()
            invoice.save()
            Invoice.update_taxes([invoice])
        return 'end'
